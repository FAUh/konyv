/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.konyvtarproject;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author viragvolgyidavid
 */
public class Book {
    
    Integer id;
    String title;
    String author;
    Integer numberOfPages;
    
    
    public void addNewBook(String title, String author, String pageNumber, String isbnNumber) throws SQLException{
       PreparedStatement ps = Connector.connection.prepareStatement("insert into Books (Title, Author, PageNumber, ISBN) values(?, ?, ?, ?)");
       ps.setString(1, title);
       ps.setString(2, author);
       ps.setString(3, pageNumber);
       ps.setString(4, isbnNumber);
       ps.execute();
    }
    
}
