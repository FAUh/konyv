/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.konyvtarproject;

    



import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class Connector {
    
    
    
    static Connection connection = null;
    
    
    void connect() throws SQLException, FileNotFoundException{
        Scanner sc = new Scanner(new File("src/main/resources/login.txt"));
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setPort(3306);
        ds.setDatabaseName("LibraryManagement");
        ds.setUser(sc.nextLine());
        ds.setPassword(sc.nextLine());
        connection = ds.getConnection();
    }
    
}
