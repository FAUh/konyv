/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.konyvtarproject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author viragvolgyidavid
 */
public class User {
    
    Integer id;
    String name;
    Date regDate;
    String eamil;
    Boolean membership;
    
   
   public void addNewUser(String User, String Email) throws SQLException{
       PreparedStatement ps = Connector.connection.prepareStatement("insert into Users (Name, DateOfReg, Email) values(?, ?, ?)");
       ps.setString(1, User);
       ps.setDate(2, new java.sql.Date(new java.util.Date().getTime()));
       ps.setString(3, Email);
       ps.execute();
   }
   
   public List<Integer> userBooks(String username) throws SQLException{
       List<Integer> books = new ArrayList<>();
       PreparedStatement ps = Connector.connection.prepareStatement("select id from User where Name = ?");
       ps.setString(1, username);
       ResultSet rs = ps.executeQuery();
       while(rs.next()){
            int bookId = rs.getInt("bookId");
            books.add(bookId);      
       
   }
       return books;
  }
}