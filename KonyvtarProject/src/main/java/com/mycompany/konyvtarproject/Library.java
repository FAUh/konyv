/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.konyvtarproject;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author viragvolgyidavid
 */
public class Library {
    
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLACK = "\u001B[30m";

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws FileNotFoundException, SQLException {

        Book book = new Book();
        User user = new User();
        Connector con = new Connector();
        
        try {
            con.connect();
        } catch (SQLException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Scanner sc = new Scanner(System.in);
        String userInput;
        do{
            boolean inputCheck = false;
            System.out.println("1 - Új felhasználó bevitele");
            System.out.println("2 - Új könyv felvétele");
            System.out.println("3 - Felhasználónál lévő könyvek");
            System.out.println("4 - Kölcsönzés adminisztráció");
            System.out.println("0 - Kilépés");
            userInput = sc.next();
            if (userInput.equals("1")){
                inputCheck = true;
                Scanner newUser = new Scanner(System.in);
                System.out.println("Kérem az új tag nevét:");
                String inputUser  = newUser.nextLine();
                System.out.println("Kérem az új tag email címét:");
                String email = newUser.nextLine();
                user.addNewUser(inputUser, email);
            }
            if (userInput.equals("2")){
                Scanner newBook = new Scanner(System.in);
                System.out.println("Kérem az új könyv címét:");
                inputCheck = true;
                String bookTitle = newBook.nextLine();
                System.out.println("... szerzőjét:");
                String bookAuthor = newBook.nextLine();
                System.out.println("... oldalának száma:");
                String pageNumber = newBook.nextLine();
                System.out.println("... és ISBN számát:");
                String isbnNumber = newBook.nextLine();
                book.addNewBook(bookTitle, bookAuthor, pageNumber, isbnNumber);
            }
            if (userInput.equals("3")){
                System.out.println("Kérem a tag nevét:");
                inputCheck = true;
            
            }
            if(userInput.equals("0")){
                inputCheck = true;
            }
            if (!inputCheck){
                System.out.println();
                System.out.println(ANSI_RED + "Nem értelmezhető input!");
                System.out.println(ANSI_BLACK + "Kérem válasszon az alábbi menüpontokból:");
                System.out.println();
            }
        }while (!userInput.equals("0"));
    }
    
}
    

